<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDailyLimitToUserRanks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_ranks', function (Blueprint $table) {
            $table->dropColumn('binary_limit');
            $table->integer('levels')->default(1);
            $table->float('daily_limit', 2,4)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_ranks', function (Blueprint $table) {
            //
        });
    }
}
