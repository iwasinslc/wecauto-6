@extends('layouts.profile')
@section('title', __('Car order'))
@section('content')
    <section class="order-step">
        <div class="container">
            <div class="car-orders">
                <h3 class="lk-title">{{__('Car order')}}
                </h3>
            </div>
            <ul class="steps-indicator">
                <li class="is-active"> <span>1</span>
                </li>
                <li class="is-active is-current"> <span>2</span>
                </li>
                <li> <span>3</span>
                </li>
                <li> <span>4</span>
                </li>
            </ul>
            <div class="steps-title">
                <h4><span class="color-warning">{{__('Step')}}</span> #2</h4><small>{{__('Add photo')}}</small>
            </div>
            <div class="steps-intro">
                <p>{{__('Add your car/moto photo')}}</p>
            </div>
            <form method="post" enctype="multipart/form-data" action="{{route('profile.deposits.add_photo_handle')}}">
                {{csrf_field()}}
            <div class="upload-component">
                <label>
                    <input name="image" required  type="file"><span class="upload-component__block"><span class="upload-component__block-content"><img src="/assets/images/plus.png" alt="">
                    <p class="upload-component__name">{{__('Upload from your device')}}
                    </p><span class="upload-component__desc">JPEG, PNG, GIF  </span></span></span>
                </label>
            </div>
            <div class="order-step__buttons">
                <button class="btn btn--warning btn--size-md" type="submit">{{__('Next step')}}</button>
                <a class="btn btn--gray btn--size-md" href="{{route('profile.deposits.add_amount')}}">Пропустить шаг</a>
            </div>
            </form>
        </div>
    </section>
    <!-- /.card -->
@endsection

@push('load-scripts')

@endpush