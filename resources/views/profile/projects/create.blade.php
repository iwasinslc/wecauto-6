@extends('layouts.profile')
@section('title', 'Предложить проект')
@section('content')
    <!-- inventor begin-->
    <div class="inventor" style="padding:30px;">
        <div class="container">
            <div class="row">
                @include('partials.inform')
                <form action="{{ route('profile.projects.verify') }}" enctype="multipart/form-data" method="POST" target="_top" style="width:100%;">
                    {{ csrf_field() }}

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Название проекта <strong>*</strong></label>
                        <div class="">
                            <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Логотип <strong>*</strong></label>
                        <div class="">
                            <input type="file" name="logotype" class="form-control">
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Контакты куратора (имя, телеграм логин) <strong>*</strong></label>
                        <div class="">
                            <textarea name="curator_contacts" class="form-control">{{ old('curator_contacts') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Ссылка на проект</label>
                        <div class="">
                            <input type="text" name="url" class="form-control" value="{{ old('url') }}">
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Ссылка на видео-описание</label>
                        <div class="">
                            <input type="text" name="video_description" class="form-control" value="{{ old('video_description') }}" placeholder="https://www.youtube.com/watch?v=eMC7HEUsYZg">
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="description">Краткое описание проекта <strong>*</strong></label>
                        <div class="">
                            <textarea name="description" class="form-control">{{ old('description') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="marketing_description">Маркетинг план <strong>*</strong></label>
                        <div class="">
                            <textarea name="marketing_description" class="form-control">{{ old('marketing_description') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">
                                Предложить проект
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- inventor end -->
    <!-- contact end -->
@endsection