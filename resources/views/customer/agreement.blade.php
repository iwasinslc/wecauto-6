@extends('layouts.auth')
@section('title', __('Agreement'))
@section('content')
    <section class="page-preview">
        <div class="container">
                                <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/</span><span>{{__('Agreement')}}</span>
            </div>
            <h1 class="page-preview__title">Пользовательское <span class="color-primary">соглашение</span>
            </h1>
        </div>
    </section>
    <section class="terms">
        <div class="container">
            <div class="typography">
                <p class="typography-intro-text"> <strong>
                        <big>Перед началом использования всех возможностей автопрограммы WECAUTO и данного веб-ресурса пользователь обязуется ознакомиться с данным Пользовательским соглашением и следовать всем его положениям. Соглашение действует с момента регистрации пользователя на сайте и обладает юридической силой на протяжении всего периода сотрудничества между клиентом и администрацией.</big></strong></p>
                <ol class="circle-number-list">
                    <li>Внесение первоначального взноса по автопрограмме WECAUTO производится исключительно в монетах WEC (Web Coin Pay), в размере 30% от общей стоимости транспортного средства в долларовом эквиваленте.</li>
                    <li>Участие в программе автокредитования допускается только после приобретения лицензии, условия и стоимость которых обозначены в маркетинг-плане WECAUTO.</li>
                    <li>Каждая лицензия имеет свой ограниченный срок действия. После окончания срока действия лицензии для дальнейшего участия в автопрограмме пользователю необходимо приобрести новую лицензию.</li>
                    <li>Согласно маркетинг-плану программы WECAUTO, по истечении срока вклада участнику начисляется 100% от стоимости транспортного средства в коинах WEC. Данная сумма зачисляется единовременно и одним платежом сразу после окончания срока действия тарифного плана.</li>
                    <li>Начисления по стандартному автодепозиту производятся после истечения 12 месяцев со дня внесения первоначального взноса. Пользователь имеет возможность сократить действие своего депозита путём приобретения фастеров FST ―растущего цифрового актива. После каждой активации FST дата окончания действия вклада смещается в сторону приближения, точную информацию о чём можно получить в личном кабинете пользователя.</li>
                    <li>Участник имеет право распоряжаться фастерами на своё усмотрение, покупая или продавая их на внутренней P2P-бирже в любом необходимом объёме.</li>
                    <li>Пользователь вправе претендовать на получение FST на бесплатной основе, в виде кэшбека от приобретения лицензии, за участие в партнёрской программе или в качестве bounty-вознаграждения за выполнение определённых целевых действий.</li>
                    <li>Курс обмена WEC/USD и в обратном направлении не зависит от администрации компании, а формируется на внутренней P2P за счёт пользовательской активности.</li>
                    <li>Участник имеет право оформлять неограниченное количество взносов на приобретение транспортных средств на базе одного личного кабинета.</li>
                    <li>Условия сотрудничества администрации и пользователей ресурса предусматривают невозможность досрочного прекращения срока действия автопрограммы, а также досрочного возврата и вывода первоначального взноса.</li>
                    <li>Пользователь данного ресурса получает доступ к партнёрской программе WECAUTO и может привлекать других лиц к сотрудничеству, за что будет получать партнёрские начисления в процентном соотношении от сумм депозитов рефералов. Размеры начислений прописаны в условиях партнёрской программы.</li>
                    <li>Участнику может быть отказано в выплате в случае, если он допускает нарушения правил сотрудничества, в частности, если он использует собственную реферальную ссылку для создания аккаунтов-клонов в целях получения финансовой выгоды в рамках партнёрской программы.</li>
                    <li>Участник автопрограммы WECAUTO может претендовать на получение каждого из рангов партнёрской программы при условии выполнения требований, предъявляемых к тому или иному рангу.</li>
                    <li>Участник обязуется предоставлять исключительно достоверную личную информацию и предпринимать со своей стороны все меры безопасности и конфиденциальности, чтобы не допустить получения третьими лицами несанкционированного доступа к своему аккаунту и финансовым средствам.</li>
                    <li>Участник обязуется использовать для автодепозита денежные средства, имеющие законное происхождение.</li>
                    <li>Данный раздел, а также другие разделы и условия сотрудничества администрация ресурса может дополнять или изменять без соответствующего уведомления участников программы. Ознакомление с последней редакцией Пользовательского соглашения является личной ответственностью каждого пользователя.</li>
                    <li>Администрация обязуется обеспечивать конфиденциальность и неразглашение персональных данных пользователя в соответствии с установленной Политикой конфиденциальности.</li>
                </ol>
            </div>
        </div>
    </section>
{{--    <article class="page">--}}
{{--        <section>--}}
{{--            <div class="container">--}}
{{--                <div class="page-top">--}}
{{--                    <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/</span><span>{{__('Agreement')}}</span>--}}
{{--                    </div>--}}
{{--                    <h1 class="page-title">{{__('Agreement')}}--}}
{{--                    </h1>--}}
{{--                </div>--}}
{{--                <div class="typography">--}}
{{--                    <p>{!! __('This user agreement is required before you can start using the platform. Ignorance of the terms of cooperation cannot serve as an argument in favor of the client in case of disputes and conflicts.') !!}</p>--}}
{{--                    <h4>{!! __('1. General') !!}</h4>--}}
{{--                    <p>{!! __('1.1. Use of functions and capabilities of the resource is possible only after the user has passed registration according to the procedure established by the administration and authorization in the personal office.') !!}</p>--}}
{{--                    <p>{!! __("1.2. The user's registration on this resource is confirmation of his/her consent to non-disclosure of all confidential and protected data (including login and password for authorization), observance of the company's copyright to the posted materials and content of the website. The information in this document is not an investment cooperation proposal, particularly for those jurisdictions that consider non-public offers/petitions illegal.") !!}</p>--}}
{{--                    <p>{!! __('1.3. The customer agrees to use this site exclusively by legal means, without going beyond the legal norms of his jurisdiction.') !!}</p>--}}
{{--                    <p>{!! __('1.4. Authorized employees of the company can make changes and amendments to the rules and agreements, change the interest of the trade commission and commission on withdrawal of USD, as well as the terms of cooperation with clients without obligatory corresponding notification. The new version of the user agreement takes effect when it is posted in this section or after a special system alert for users. At the same time, familiarization with current rules of work and user agreement is the personal responsibility of the user.') !!}</p>--}}
{{--                    <h4>{!! __('2. Purchase ACC') !!}</h4>--}}
{{--                    <p>{!! __('2.1. The direct purpose of ACC accelerators is to activate the accelerated interest accrual mechanism in the WEC cryptocurrency stealing system.') !!} </p>--}}
{{--                    <p>{!! __('2.2. We do not guarantee the instant acquisition of ACC after the purchase order is issued.') !!}</p>--}}
{{--                    <p>{!! __('2.3. The company does not guarantee or facilitate the instant sale of the ACС, the speed of the transaction completion depends solely on the participants of the system.') !!}</p>--}}
{{--                    <p>{!! __('The internal exchange of purchases and sales of ACC is a platform for transactions between holders of AСС. We only provide a P2P-Exchanger for purchases and sales of AСС between users, without being responsible for the transactions under the ACC.') !!} </p>--}}
{{--                    <p>{!! __('2.4. The AСС value at the time of the purchase requisition creation and the value of ACC at the time of transaction conclusion may vary depending on the current market price of ACC.') !!}</p>--}}
{{--                    <p>{!! __('2.5. ACC is not a commercial entity and does not guarantee profit.') !!}</p>--}}
{{--                    <h4>{!! __('3. The cost of WEC licenses and coins') !!}</h4>--}}
{{--                    <p>{!! __('3.1. The Company reserves the right to change the value of licenses offered for purchase.') !!}</p>--}}
{{--                    <p>{!! __('3.2. The Company has the right to make changes to the terms of the referral program, including the amount of interest charges to the partners for the targeted actions of their referrals.') !!}</p>--}}
{{--                    <p>{!! __('3.3. The company does not guarantee the sale of WEC at a specific price. Since all WEC purchase and sale transactions are carried out on third-party cryptocurrency exchanges, the value of WEC coins in dollar (or in another fiat currency) equivalent is established on the basis of market demand and supply.') !!}</p>--}}
{{--                    <h4>{!! __('4. Field of responsibility') !!}</h4>--}}
{{--                    <p>{!! __('4.1. The Company shall not be liable for any actual or indirect losses of the system participant incurred as a result of:') !!}</p>--}}
{{--                    <ul>--}}
{{--                        <li>{!! __('personally made decisions on the use of some WEC cryptocurrency and ACC accelerators;') !!}</li>--}}
{{--                        <li>{!! __('technical problems on the part of the user (e.g. erroneous payment details) or incorrect operation of payment systems;') !!}</li>--}}
{{--                        <li>{!! __("blocking of the user's account due to financial or reputational damage to the company, violation of the rules of this user agreement, violation of the rules of the partner program or conducting other fraudulent activities on the company's website;") !!}</li>--}}
{{--                        <li>{!! __('force majeure.') !!}</li>--}}
{{--                    </ul>--}}

{{--                    <p>{!! __('4.2. The company is responsible for:') !!}</p>--}}
{{--                    <ul>--}}
{{--                        <li>{!! __("ensuring the site's health, operative resolution of any technical and software problems and errors;") !!}</li>--}}
{{--                        <li>{!! __('protection of financial resources and personal data of users from loss and unauthorized access by third parties;') !!}</li>--}}
{{--                        <li>{!! __('providing access to all sections of the site necessary for financial activities;') !!}</li>--}}
{{--                        <li>{!! __('compliance with financial obligations to clients and partners in accordance with the proposed terms of cooperation;') !!}</li>--}}
{{--                        <li>{!! __('providing clients with the opportunity to independently manage their funds by entering funds into the account balance and withdrawing funds to these electronic purses, as well as making reinvestments to increase income.') !!}</li>--}}
{{--                    </ul>--}}

{{--                    <h4>{!! __('5. Support') !!}</h4>--}}
{{--                    <p>{!! __('5.1. Technical Support is available to help you resolve issues and problems with the system members. All contact information is published in the appropriate section.') !!}</p>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
{{--    </article>--}}
@endsection