<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/assets/css/common.css?v={{rand(1,10000000)}}">
    <link rel="stylesheet" href="/assets/css/app.css?v={{rand(1,10000000)}}">
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script>
        window.__lc = window.__lc || {};
        window.__lc.license = 12063651;
        ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
    </script>
    <noscript><a href="https://www.livechatinc.com/chat-with/12063651/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
    <!-- End of LiveChat code -->
    <!-- End of LiveChat code -->
</head>
<body>
<div class="loader"><img src="/assets/images/spinner.svg" alt=""></div>







<main class="wrapper" id="app" data-emergence="hidden">

    <div class="auth">




        <header class="header">
            <div class="header__buttons-bg">
            </div>
            <div class="container">
                <div class="header__row">
                    <div class="header__col"><a class="header__logo" href="{{ route('customer.main') }}"><img
                                    src="/assets/images/logo.png?v=2" alt=""></a>
                    </div>
                    <div class="header__col">
                        <ul class="header__buttons">
                            @if (isUserAuthorized())
                                <li><a class="icon-button" href="{{route('profile.profile')}}">
                                        <svg class="svg-icon">
                                            <use href="/assets/icons/sprite.svg#icon-lock"></use>
                                        </svg>
                                        <span>{{__('Account')}}</span></a>
                                </li>
                            @else

                                <li><a class="icon-button" href="{{route('login')}}">
                                        <svg class="svg-icon">
                                            <use href="/assets/icons/sprite.svg#icon-lock"></use>
                                        </svg>
                                        <span>{{__('Login')}}</span></a>
                                </li>

                            @endif
                            <li class="separator">
                            </li>
                            <li>
                                <div class="language-module js-dropdown">
                                    <div class="language-module__link">
                                        <div class="language-module__image"><img src="/assets/images/country/{{app()->getLocale()}}.png"
                                                                                 alt="">
                                        </div>
                                        <span>{{strtoupper(app()->getLocale())}}</span>
                                    </div>
                                    <ul class="language-module__dropdown">
                                        @foreach(getLanguagesArray() as $key => $item)
                                            <li>
                                                <a href="{{ route('set.lang',['locale'=>$item['code']]) }}">
                                                        <span class="language-module__image">
                                                                                                                                    <img src="/assets/images/country/{{$item['code']}}.png"
                                                                                                                                         alt="">
                                                                                                                                </span>
                                                    <span>{{$item['name']}}</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="header__col">
                        <nav class="header__nav">
                            <ul>
                                {{--                                    <li><a href="clients.html">Clients</a>--}}
                                {{--                                    </li>--}}
                                {{--                                    <li><a href="partners.html">Partners</a>--}}
                                {{--                                    </li>--}}
                                <li class="{{ (Route::is('customer.about-us') ? 'active' : '') }}"><a
                                            href="{{route('customer.about-us')}}">{{__('Marketing')}}</a>
                                </li>
                                <li class="{{ (Route::is('customer.faq') ? 'active' : '') }}"><a
                                            href="{{route('customer.faq')}}">{{__('FAQ')}}</a>
                                </li>
                                <li class="{{ (Route::is('customer.news.*') ? 'active' : '') }}"><a
                                            href="{{route('customer.news.index')}}">{{__('News')}}</a>
                                </li>

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>

        @yield('content')
        <footer class="footer">
            <div class="container">
                <div class="footer__row">
                    <div class="footer__col"><a class="footer__logo" href="{{route('customer.main')}}"><img
                                    src="/assets/images/logo-gray.png?v=2" alt=""></a>
                        <nav class="footer__nav">
                            <ul>
                                {{--                                <li class="{{ (Route::is('customer.about-us') ? 'active' : '') }}"><a--}}
                                {{--                                            href="{{route('customer.about-us')}}">{{__('Marketing')}}</a>--}}
                                {{--                                </li>--}}
                                {{--                                <li class="{{ (Route::is('customer.faq') ? 'active' : '') }}"><a--}}
                                {{--                                            href="{{route('customer.faq')}}">{{__('FAQ')}}</a>--}}
                                {{--                                </li>--}}
                                {{--                                <li class="{{ (Route::is('customer.support') ? 'active' : '') }}"><a--}}
                                {{--                                            href="{{route('customer.support')}}">{{__('Contacts')}}</a>--}}
                                {{--                                </li>--}}
                            </ul>
                        </nav>
                    </div>

                </div>
                <div class="footer__bottom">
                    <p>© 2020, WEC Auto. {{__('All rights reserved')}}.</p>
                    <ul>
                        <li><a href="{{route('customer.agreement')}}">{{__('Agreement')}}</a></li>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</main>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="/assets/js/vendors.js?v={{rand(1,10000000)}}"></script>
<script src="/assets/js/app.js?v={{rand(1,10000000)}}"></script>
<script>
    function refreshCaptcha() {
        $.ajax({
            url: "/refereshcapcha",
            type: 'get',
            dataType: 'html',
            success: function (json) {
                $('.captcha').html(json);
            },
            error: function (data) {
                alert('Try Again.');
            }
        });
    }
</script>

<script>
    @if(session()->has('success'))
    window.Notice.openSuccess('{{session()->get('success')}}', 5000);
    @endif

    @if(session()->has('status'))
    window.Notice.openSuccess('{{session()->get('status')}}', 5000);
    @endif

    @if(session()->has('error'))
    window.Notice.openError('{{session()->get('error')}}', 5000);
    @endif

    @if ($errors->any())


    @foreach ($errors->all() as $error)
    window.Notice.openError('{{$error}}', 5000);
    @endforeach

    @endif
</script>

@stack('load-scripts')


</body>

</html>